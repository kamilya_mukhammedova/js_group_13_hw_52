import { Component } from '@angular/core';
import { CardDeck } from "../lib/cardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'homework52-app';
  cardDeck;
  cardsInGame;

  constructor() {
    this.cardDeck = new CardDeck();
    this.cardsInGame = this.cardDeck.getCards(2);
  }

  giveAnotherCard() {
   this.cardsInGame.push(this.cardDeck.getCard());
   return this.cardsInGame;
  }

  reset() {
      this.cardsInGame = [];
      this.cardDeck = new CardDeck();
      this.cardsInGame = this.cardDeck.getCards(2);
  }

  getTotalScore() {
    let score = 0;
    for(let i = 0; i < this.cardsInGame.length; i++) {
      score += this.cardsInGame[i].getScore();
    }
    return score;
  }

  getMessage() {
    if(this.getTotalScore() === 21) {
      return 'You win!';
    } else if(this.getTotalScore() > 21) {
      return 'You lose!';
    }
    return 'Game in progress...';
  }

  getDisabled() {
    return this.getTotalScore() >= 21;
  }
}
