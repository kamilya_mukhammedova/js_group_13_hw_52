import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent  {
  @Input() rank = '';
  @Input() suit = '';

 getSuit() {
   if(this.suit === 'diams') {
     return '♦';
   } else if (this.suit === 'hearts') {
     return '♥';
   } else if (this.suit === 'clubs') {
     return '♣';
   } else if (this.suit === 'spades') {
     return '♠';
   }
   return '';
 }

 getClassName () {
   return `card rank-${this.rank.toLowerCase()} ${this.suit}`;
 }
}
