const ranksArray = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const suitsArray = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  constructor(public rank:string, public suit:string) {}

  getScore():number {
    if(this.rank === 'J' || this.rank === 'Q' || this.rank === 'K') {
      return 10;
    } else if(this.rank === 'A') {
      return 11;
    } else if(parseInt(this.rank) >= 2 && parseInt(this.rank) <= 10) {
      return parseInt(this.rank);
    }
    return 0;
  }
}

export class CardDeck {
  cards: Card[] = [];

  constructor() {
    for(let i = 0; i < ranksArray.length; i++) {
      for(let j = 0; j < suitsArray.length; j++) {
        this.cards.push(new Card(ranksArray[i], suitsArray[j]));
      }
    }
  }

  getCard(): Card {
    const randomCard = this.cards[Math.floor(Math.random() * this.cards.length)];
    const indexOfRandomCard = this.cards.indexOf(randomCard);
    this.cards.splice(indexOfRandomCard, 1);
    return randomCard;
  }

  getCards(howMany: number): Card[] {
    const arrayNumberCards = [];
    for(let i = 0; i < howMany; i++) {
       arrayNumberCards.push(this.getCard());
    }
    return arrayNumberCards;
  }
}
